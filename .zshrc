# --- Алиасы ---
alias reboot='sudo reboot'
alias poweroff='sudo poweroff'
alias powersleep='sudo s2disk'
alias lsh='ls -lAh --color=always --group-directories-first'
alias ls='ls --color=always --group-directories-first'
alias grep='grep --color=always'


# --- Настройки ---
HISTFILE=$HOME/.zsh_history    # История
HISTSIZE=9999
SAVEHIST=9999
setopt inc_append_history
setopt share_history
setopt hist_ignore_all_dups
setopt hist_reduce_blanks

setopt autocd     # Автопереход

#setopt CORRECT_ALL     # Исправление ввода

# подсветка синтаксиса
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
ZSH_HIGHLIGHT_HIGHLIGHTERS=(main brackets pattern)
ZSH_HIGHLIGHT_PATTERNS+=('rm -rf' 'fg=black,bg=yello')
ZSH_HIGHLIGHT_PATTERNS+=('rm -rf \*' 'fg=black,bg=red')
ZSH_HIGHLIGHT_PATTERNS+=('rm \* -rf' 'fg=black,bg=red')
SPROMPT="%B%F{red}Внимание!%f%b Вместо %B%F{yellow}%R%f%b хотел %B%F{yellow}%r%f%b ? (%BY%b/%BN%b/%BE%b/%BA%b) "

autoload -U compinit     # Автокомплит
compinit
zmodload zsh/complist
zstyle ':completion:*' menu yes select
zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}


# --- Привествие ---
# Состояние контроля версий
autoload -Uz vcs_info
zstyle ':vcs_info:*' enable svn git
zstyle ':vcs_info:*' check-for-changes true
zstyle ':vcs_info:*' get-revision true
zstyle ':vcs_info:*' stagedstr "+"
zstyle ':vcs_info:*' unstagedstr "!"
zstyle ':vcs_info:*' formats '[%s:%b%F{red}%c%u%F{green}]'
setopt PROMPT_SUBST
precmd () { 
    vcs_info 
    #HG="$(hg_branch)$(hg_dirty)" #Mercurial, но как же он всё тормазит
}
#PROMPT='%B%F{yellow}%n%F{green}${HG}${vcs_info_msg_0_}%F{blue}%(!.#.$)%f%b '
PROMPT='%B%F{yellow}%n%F{green}${vcs_info_msg_0_}%F{blue}%(!.#.$)%f%b '
# Приветсвие - текущий путь
RPROMPT='[%~]'

# --- Заголовок окна ---
# Выводится последняя выполненая команда
case $TERM in *rxvt*|*term|linux) 
    preexec () { print -Pn "\e]0;urxvt: $1\a" }
    ;; 
esac


# --- Не чёткий поиск по истории ---
# взято от сюда http://reangdblog.blogspot.com/2016/07/blog-post_19.html
#  "fc -lnr 1" - возвращает инвертированный список всех ранее введённых команд
# опция "–tiebreak=begin" для fzf - при сортировке результатов, предпочитать строки с совпадениями ближе к началу
# всё остальное это специфичный для zsh способ записать результат в строку редактирования и навесить на это горячую клавишу ctrl+R
fzf-history-widget() {
  LBUFFER=$(fc -lnr 1 | fzf --tiebreak=begin)
  zle redisplay
}
zle -N fzf-history-widget


# --- Закладок ---
# взято от сюда http://reangdblog.blogspot.com/2016/07/blog-post_19.html
function fzf-bookmarks-widget() {
  cd $(cat "$HOME/.config/term_bookmarks.cfg" | fzf --tiebreak=begin --tac | awk '{print $2}')
  zle reset-prompt
}
zle -N fzf-bookmarks-widget


# --- Настройка горячих клавиш --- 
# код клавиш: cat >/dev/null
bindkey "${terminfo[khome]}" beginning-of-line
bindkey "${terminfo[kend]}" end-of-line
bindkey "^[[3~" delete-char
bindkey "^[Oc" forward-word
#bindkey "eOc" emacs-forward-word
bindkey "^[Od" backward-word
#bindkey "eOd" emacs-backward-word
# нечёткий поиск по истории
bindkey "^[Oa" fzf-history-widget
bindkey '^R' fzf-history-widget
# чёткий поиск по истории: вводим начало команды, а дальше стрелачками перебираем варианты
bindkey '^[[A' history-beginning-search-backward    
bindkey '^[[B' history-beginning-search-forward 
# Закладок
bindkey '^B' fzf-bookmarks-widget
